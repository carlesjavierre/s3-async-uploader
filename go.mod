module gitlab.com/carlesjavierre/s3-async-uploader

go 1.14

require (
	github.com/aws/aws-sdk-go v1.33.7
	github.com/urfave/cli/v2 v2.2.0
)
