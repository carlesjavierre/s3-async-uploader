package main

import (
	"bytes"
	"fmt"
	"log"
	"net/http"
	"os"
	"sync"
	"time"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
	"github.com/urfave/cli/v2"
)

var (
	awsRegion   = "eu-west-1"
	awsBucket   = ""
	awsFolder   = "/"
	concurrency = 150
)

func main() {

	cli.VersionPrinter = func(c *cli.Context) {
		fmt.Printf("Version: %s\n", c.App.Version)
	}
	app := &cli.App{
		Name:                 "s3-async-uploader",
		Usage:                "Multi threaded utility that uploads and categorizes files to S3 which are contained in origin on a single folder, formatting them into folders by date (YYYY/MM/DD)",
		UsageText:            "s3-async-uploader --s3bucket bucket --s3folder / --backupfolder /path/to/data/",
		Version:              "0.1.0",
		EnableBashCompletion: true,

		Authors: []*cli.Author{
			&cli.Author{
				Name:  "Carles Javierre",
				Email: "info@carlesjavierre.com",
			},
		},

		Flags: []cli.Flag{
			&cli.StringFlag{
				Name:  "s3bucket",
				Value: "",
				Usage: "S3 Bucket",
			},
			&cli.StringFlag{
				Name:  "s3folder",
				Value: "/",
				Usage: "S3 Destination folder (end with \"/\")",
			},
			&cli.StringFlag{
				Name:  "s3region",
				Value: "eu-west-1",
				Usage: "S3 Region (defaults to eu-west-1)",
			},
			&cli.StringFlag{
				Name:  "backupfolder",
				Value: "",
				Usage: "Full path to the folder that needs to be uploaded",
			},
			&cli.StringFlag{
				Name:  "concurrency",
				Value: "150",
				Usage: "Max simultaneous concurrency (goroutines)",
			},
		},

		Action: func(c *cli.Context) error {
			initialize(c)
			return nil
		},
	}

	app.Run(os.Args)
}

func initialize(c *cli.Context) {

	// Required
	if c.String("backupfolder") == "" {
		fmt.Println("Please set the backupfolder flag.")
		os.Exit(1)
	}
	if c.String("s3bucket") == "" {
		fmt.Println("Please set the s3bucket flag.")
		os.Exit(1)
	} else {
		awsBucket = c.String("s3bucket")
	}
	if c.String("s3folder") == "" {
		fmt.Println("Please set the s3folder flag.")
		os.Exit(1)
	} else {
		awsFolder = c.String("s3folder")
	}

	// Required With Defaults
	if c.String("s3region") != "" {
		awsRegion = c.String("s3region")
	}
	if c.Int("concurrency") < 1 {
		fmt.Println("Please, set the concurrency to at least 1. Default is 150.")
		os.Exit(1)
	} else {
		concurrency = c.Int("concurrency")
	}

	parseDir(c.String("backupfolder"))

}

func parseDir(dir string) {
	fileList := searchDir(dir)

	wg := sync.WaitGroup{}
	wg.Add(len(fileList))

	fmt.Println("Starting loop")
	start := time.Now()
	currentItem := 0

	var guard = make(chan struct{}, concurrency) // Concurrency
	for i := 0; i < len(fileList); i++ {
		guard <- struct{}{}
		go func(i int) {
			defer wg.Done()
			fileDate := getFileDate(dir + fileList[i])
			getFileDate(dir + fileList[i])
			err := addFileToS3(sess, dir, fileList[i], fileDate, fileList[i])
			if err != nil {
				log.Fatal(err)
			}
			// fmt.Printf("%d\n", i)
			// fmt.Println(fileDate + "/" + fileList[i])
			currentItem++
			<-guard
		}(i)
		fmt.Printf("Current Item: %d of %d (%v%%)\n", currentItem, len(fileList), 100*currentItem/len(fileList))
	}
	fmt.Println("Finished loop. Waiting for all threads to exit...")
	wg.Wait()

	fmt.Println("Loop finished")
	elapsed := time.Since(start)
	log.Printf("Loop took %s", elapsed)
}

func searchDir(dirname string) []string {

	var list []string

	f, err := os.Open(dirname)
	if err != nil {
		log.Fatal(err)
	}
	files, err := f.Readdir(-1)
	f.Close()
	if err != nil {
		log.Fatal(err)
	}

	for _, file := range files {
		if file.IsDir() {
			continue
		}
		list = append(list, file.Name())
	}
	return list
}

func getFileDate(filename string) string {

	file, err := os.Stat(filename)

	if err != nil {
		fmt.Println(err)
	}

	modifiedtime := file.ModTime().String()

	const (
		layoutFULL   = "2006-01-02 15:04:05.999999999 Z0700 MST"
		layoutFOLDER = "2006/01/02"
	)

	t, err := time.Parse(layoutFULL, modifiedtime)
	if err != nil {
		fmt.Println(err)
	}
	return t.Format(layoutFOLDER)
}

var sess = connectAWS()

func connectAWS() *session.Session {
	sess, err := session.NewSession(&aws.Config{Region: aws.String(awsRegion)})
	if err != nil {
		panic(err)
	}
	return sess
}

func addFileToS3(s *session.Session, fileDir string, fileName string, targetDir string, targetName string) error {

	// Open the file for use
	file, err := os.Open(fileDir + fileName)
	if err != nil {
		return err
	}
	defer file.Close()

	// Get file size and read the file content into a buffer
	fileInfo, _ := file.Stat()
	var size int64 = fileInfo.Size()
	buffer := make([]byte, size)
	file.Read(buffer)

	// Config settings: this is where you choose the bucket, filename, content-type etc.
	// of the file you're uploading.
	_, err = s3.New(s).PutObject(&s3.PutObjectInput{
		Bucket:               aws.String(awsBucket),
		Key:                  aws.String(awsFolder + targetDir + "/" + targetName),
		ACL:                  aws.String("private"),
		Body:                 bytes.NewReader(buffer),
		ContentLength:        aws.Int64(size),
		ContentType:          aws.String(http.DetectContentType(buffer)),
		ContentDisposition:   aws.String("attachment"),
		ServerSideEncryption: aws.String("AES256"),
	})
	return err
}
